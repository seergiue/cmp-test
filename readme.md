# TeamCMP Full-Stack Test

## Description

This test is wrapped in a Laravel application but it only uses the routing.

All the logic is inside `project/src` which is built as an external logic in mind, meaning you could copy the src folder and apply it to any other PHP project.

`project/src/API`

All API logic in DDD architecture.

`project/src/ORM`

Custom light weight ORM (since Eloquent is detached from User model).

`project/src/Database`

Basic Database Adapter Interface and DummyDB Adapter class.

## Setup

Setup the infrastructure

```
$ docker/setup
```

`docker/setup` performs the following tasks:

- Builds the containers
- Sets up .env file
- Installs composer dependencies
- Installs node modules through yarn
- Compiles and generates assets
- Generates artisan key

The Chat is exposed at `http://127.0.0.1:1912`

## API Endpoint

The API is exposed at `http://127.0.0.1:1912/api`

|Type|Endpoint|Description|
|---|---|---|
|GET|/api/users/{id}|Returns a single user|

## Console

To access the machine run the following:

```
$ docker/console <command-to-execute>
```

## Testing

To run functional and unit tests, run the following command:
```
$ docker/test
```

## Database

The database is exposed at `127.0.0.1:3305`

## Extra

- MariaDB 10.5.6
- Laravel 8.11.2
- PHP-FPM 7.4
- Composer 1.10.15
- Nginx 1.19.3
- Node.js 15.x