<?php


namespace App\Http\Controllers\Chat;


use App\Http\Controllers\Controller;

class ShowProfileController extends Controller
{
    public function __invoke()
    {
        return view('chat.pages.profile');
    }
}
