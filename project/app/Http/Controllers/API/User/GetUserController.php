<?php

namespace App\Http\Controllers\API\User;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\Routing\ResponseFactory;
use Src\API\User\Infrastructure\GetUserController as GetUserControllerAlias;

class GetUserController extends Controller
{
    /**
     * @var GetUserControllerAlias
     */
    private $getUserController;

    /**
     * GetUserController constructor.
     * @param GetUserControllerAlias $getUserController
     */
    public function __construct(GetUserControllerAlias $getUserController)
    {
        $this->getUserController = $getUserController;
    }

    /**
     * @param Request $request
     * @return Application|ResponseFactory|Response
     */
    public function __invoke(Request $request)
    {
        $user = new UserResource($this->getUserController->__invoke($request));
        return response($user, 200);
    }
}
