<?php

namespace Src\Database;

use Src\ORM\Model\Model;

/**
 * Interface AdapterInterface
 *
 * Adapter Interface for DB connection. We would have methods like select, insert, update, etc but for demo purposes only "find" was created
 *
 * @package Src\Database
 */
interface AdapterInterface
{
    public function find(Model $model, int $id);
}
