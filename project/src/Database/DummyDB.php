<?php


namespace Src\Database;


use Illuminate\Database\Eloquent\ModelNotFoundException;
use Src\ORM\Model\Model;

class DummyDB implements AdapterInterface
{
    /**
     * Fake User table for demo purposes
     * @var array[]
     */
    private $users = [
        [
            'id'   => 1,
            'name' => 'Sophie'
        ],
        [
            'id'   => 2,
            'name' => 'Mark',
        ],
        [
            'id'   => 3,
            'name' => 'Sarah'
        ]
    ];

    /**
     * @param Model $model
     * @param int $id
     * @return mixed|null
     * @throws \ReflectionException
     */
    public function find(Model $model, int $id)
    {
        if (property_exists(self::class, $model->getTableName())) {
            foreach ($this->{$model->getTableName()} as $modelObject) {
                if ($modelObject['id'] === $id) {
                    return $modelObject;
                }
            }

            return null;
        }

        throw new \Exception('Table name "' . $model->getTableName() . '" for class ' . $model->getClassName() . ' not found');
    }
}
