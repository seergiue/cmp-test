<?php

namespace Src\API\User\Domain\Contracts;

use Src\API\User\Domain\User;
use Src\API\User\Domain\ValueObjects\UserId;

interface UserRepositoryContract
{
    /**
     * @param UserId $id
     * @return User|null
     */
    public function find(UserId $id): ?User;
}
