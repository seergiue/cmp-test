<?php

namespace Src\API\User\Domain\ValueObjects;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

final class UserName
{
    /**
     * @var string
     */
    private $value;

    /**
     * UserName constructor.
     * @param string|null $name
     * @throws ValidationException
     */
    public function __construct(?string $name)
    {
        $this->validate($name);
        $this->value = $name;
    }

    /**
     * @return string
     */
    public function value(): string
    {
        return $this->value;
    }

    /**
     * @param string|null $name
     * @throws ValidationException
     */
    private function validate(?string $name): void
    {
        $validator = Validator::make(
            ['name' => $name],
            ['name' => 'required|string|max:40']
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }
}
