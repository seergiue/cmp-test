<?php

namespace Src\API\User\Domain\ValueObjects;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

final class UserId
{
    /**
     * @var int
     */
    private $value;

    /**
     * UserId constructor.
     * @param int $id
     * @throws ValidationException
     */
    public function __construct(int $id)
    {
        $this->validate($id);
        $this->value = $id;
    }

    /**
     * @param int $id
     * @throws ValidationException
     */
    private function validate(int $id): void
    {
        $validator = Validator::make(
            ['id' => $id],
            ['id' => 'sometimes|integer']
        );

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }

    /**
     * @return int
     */
    public function value(): int
    {
        return $this->value;
    }
}
