<?php

namespace Src\API\User\Domain;

use Src\API\User\Domain\ValueObjects\UserId;
use Src\API\User\Domain\ValueObjects\UserName;

final class User
{
    /**
     * @var UserId|null
     */
    private $id;

    /**
     * @var UserName
     */
    private $name;

    /**
     * User constructor.
     * @param UserId|null $id
     * @param UserName $name
     */
    public function __construct(
        ?UserId $id,
        UserName $name
    )
    {
        $this->id      = $id;
        $this->name    = $name;
    }

    /**
     * @return UserId|null
     */
    public function id(): ?UserId
    {
        return $this->id;
    }

    /**
     * @return UserName
     */
    public function name(): UserName
    {
        return $this->name;
    }

    /**
     * @param UserId|null $id
     * @param UserName $name
     * @return User
     */
    public static function create(
        ?UserId $id,
        UserName $name
    ): User
    {
        return new self($id, $name);
    }
}
