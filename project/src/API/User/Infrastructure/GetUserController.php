<?php

namespace Src\API\User\Infrastructure;

use Illuminate\Http\Request;
use Src\API\User\Application\GetUserUseCase;
use Src\API\User\Infrastructure\Repositories\UserRepository;

final class GetUserController
{
    /**
     * @var UserRepository
     */
    private $repository;

    /**
     * GetUserController constructor.
     * @param UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function __invoke(Request $request)
    {
        $userId = (int)$request->id;

        $getUserUseCase = new GetUserUseCase($this->repository);
        return $getUserUseCase->__invoke($userId);
    }
}
