<?php

namespace Src\API\User\Infrastructure\Repositories;

use App\Models\User as UserModel;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Src\API\User\Domain\Contracts\UserRepositoryContract;
use Src\API\User\Domain\User;
use Src\API\User\Domain\ValueObjects\UserId;
use Src\API\User\Domain\ValueObjects\UserName;

final class UserRepository implements UserRepositoryContract
{
    /**
     * @var UserModel
     */
    private $userModel;

    /**
     * UserRepository constructor.
     */
    public function __construct()
    {
        $this->userModel = new UserModel;
    }

    /**
     * @param UserId $id
     * @return User|null
     * @throws \Illuminate\Validation\ValidationException
     */
    public function find(UserId $id): ?User
    {
        $user = $this->userModel->find($id->value());

        if ($user === null) {
            throw new ModelNotFoundException;
        }

        return new User(
            new UserId($user['id']),
            new UserName($user['name'])
        );
    }
}
