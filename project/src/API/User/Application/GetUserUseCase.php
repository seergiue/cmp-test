<?php

namespace Src\API\User\Application;

use Illuminate\Validation\ValidationException;
use Src\API\User\Domain\Contracts\UserRepositoryContract;
use Src\API\User\Domain\ValueObjects\UserId;

final class GetUserUseCase
{
    /**
     * @var UserRepositoryContract
     */
    private $repository;

    /**
     * GetUserUseCase constructor.
     * @param UserRepositoryContract $repository
     */
    public function __construct(UserRepositoryContract $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param int $userId
     * @return mixed
     * @throws ValidationException
     */
    public function __invoke(int $userId)
    {
        $id = new UserId($userId);

        return $this->repository->find($id);
    }
}
