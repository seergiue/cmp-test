<?php


namespace Src\ORM\Model;


interface ModelInterface
{
    public function find(int $id);
}
