<?php

namespace Src\ORM\Model;

use Src\Database\AdapterInterface;
use Src\Database\DummyDB;

class Model implements ModelInterface
{
    /**
     * @var AdapterInterface
     */
    private $adapter;

    /**
     * @var string
     */
    protected $table;

    /**
     * @var \ReflectionClass
     */
    private $reflection;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $this->reflection = new \ReflectionClass($this);
        $this->boot();
    }

    protected function boot()
    {
        $this->adapter = new DummyDB();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function find(int $id)
    {
        try {
            return $this->adapter->find($this, $id);
        } catch (\Exception $e) {
            die($e->getMessage());
        }
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    final public function getShortClassName()
    {
        return $this->reflection->getShortName();
    }

    /**
     * @return string
     * @throws \ReflectionException
     */
    final public function getClassName()
    {
        return $this->reflection->getName();
    }

    /**
     * @return string
     */
    final public function getTableName()
    {
        return $this->table;
    }
}
