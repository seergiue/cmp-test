class Chat {

    newMessage(itsMe, msg) {
        if (typeof msg !== 'undefined' && msg.trim().length > 0) {
            let chatContent = document.getElementById('chatContent');
            chatContent.innerHTML += this.getMessageHtml(itsMe, msg)
            document.getElementById('inputMsg').value = ''
            chatContent.scrollTop = chatContent.scrollHeight
        }
    }

    getMessageHtml(itsMe, msg) {
        if (itsMe) {
            return `<div class="flex flex-row space-x-4 justify-end">
                        <div class="ml-4 message bg-blue-500 flex justify-center p-4 text-white break-all">${msg}</div>
                        <div class="w-12 h-12 rounded-full bg-cover bg-center bg-no-repeat flex-shrink-0" style="background-image: url('https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80')"></div>
                    </div>`;
        } else {
            return `<div class="flex flex-row space-x-4 justify-start">
                        <div class="w-12 h-12 rounded-full bg-cover bg-center bg-no-repeat flex-shrink-0" style="background-image: url('https://images.unsplash.com/photo-1566492031773-4f4e44671857?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80')"></div>
                        <div class="ml-4 message bg-white flex justify-center p-4 break-all">${msg}</div>
                    </div>`;
        }
    }
}

window.chat = new Chat();
setTimeout(() => {
    window.chat.newMessage(false, 'Hello how are you?')
}, 1000)

document.getElementById('inputMsg').addEventListener('keyup', (event) => {
    if (event.keyCode === 13) {
        event.preventDefault();
        window.chat.newMessage(true, document.getElementById('inputMsg').value)
    }
})
