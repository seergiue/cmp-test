require('./bootstrap');

let addFriend = document.getElementById('addFriend');

if (addFriend !== null) {
    addFriend.addEventListener('click', toggleTheme)
}

function toggleTheme() {
    let body = document.body;

    if (body.classList.contains('theme-light')) {
        body.classList.remove('theme-light')
        body.classList.add('theme-dark')
    } else {
        body.classList.remove('theme-dark')
        body.classList.add('theme-light')
    }
}
