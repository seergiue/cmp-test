<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Chat App</title>

    <link href="{{ mix('css/chat/bundle.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body class="h-screen theme-light">
    @yield('content')
</body>
<script src="{{ mix('js/app.js') }}" defer></script>
<script src="https://kit.fontawesome.com/15036f2445.js" crossorigin="anonymous"></script>
@stack('scripts')
</html>
