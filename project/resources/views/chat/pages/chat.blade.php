@extends('layouts.chat')

@section('content')
    <div class="content w-full h-full">
        <div class="top-bar">
            <a href="{{ route('chat.profile') }}" class="px-4 flex items-center justify-center h-full absolute top-0 right-0">
                <div class="w-12 h-12 rounded-full bg-cover bg-center bg-no-repeat" style="background-image: url('https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80')"></div>
            </a>
            Sophie
        </div>
        <div class="h-full py-8 px-4 overflow-auto flex flex-col space-y-4" id="chatContent">
        </div>
        <div class="h-auto flex p-4 space-x-2">
            <div class="w-full">
                <input type="text" class="w-full border border-gray-300 h-full px-2 outline-none" placeholder="Write a message..." id="inputMsg">
            </div>
            <button class="button w-auto bg-blue-500 px-4 py-3 text-white font-medium uppercase text-sm outline-none" onclick="window.chat.newMessage(true, document.getElementById('inputMsg').value)">Send</button>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="{{ mix('js/chat.js') }}" defer></script>
@endpush
