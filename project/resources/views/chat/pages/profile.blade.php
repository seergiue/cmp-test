@extends('layouts.chat')

@section('content')
    <div class="content">
        <div class="top-bar">
            <a href="{{ route('chat.room') }}" class="px-6 h-full flex items-center justify-center absolute top-0 left-0">
                <i class="fas fa-arrow-left"></i>
            </a>
            <h1>Sophie</h1>
        </div>
        <div class="h-full p-8 overflow-auto">
            <div class="relative m-auto flex justify-center w-56 h-56 rounded-full bg-cover bg-center bg-no-repeat" style="background-image: url('https://images.unsplash.com/photo-1529626455594-4ff0802cfb7e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=634&q=80')">
                <div class="bg-green-400 uppercase flex absolute w-16 h-16 text-white items-center justify-center rounded-full top-0 right-0 text-sm">online</div>
            </div>
            <div class="py-2">
                <h2 class="text-2xl text-color">Sophie, 22</h2>
                <h3 class="text-pink-500 text-sm font-medium">New York</h3>
                <p class="text-sm mt-4 text-color">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec quis ipsum non felis volutpat condimentum. Nunc tempor ipsum sit amet ex sollicitudin fringilla. Mauris bibendum ex velit, sed blandit neque aliquam et.</p>
            </div>
        </div>
        <div class="h-auto p-4">
            <button id="addFriend" class="button outline-none p-4 w-full uppercase text-white text-sm">Add as a friend</button>
        </div>
    </div>
@endsection
