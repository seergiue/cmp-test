<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GetUserTest extends TestCase
{
    public function testGetUser()
    {
        $this->getJson(route('users.find', ['id' => 1]))
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'name'
                ]
            ]);
    }

    public function testUserIsNotFound()
    {
        $this->getJson(route('users.find', ['id' => 0]))
            ->assertStatus(404)
            ->assertJsonStructure([
                'message',
                'code'
            ]);
    }
}
